from django.contrib.auth import get_user_model

from rest_framework.test import APILiveServerTestCase, APIClient

User = get_user_model()

class RegistrationTestCase(APILiveServerTestCase):
    def test_registration_fields_required(self):
        client = APIClient()
        path = '/auth/users/'
        data = {
            'email': 'jdoe@example.com',
        }
        response = client.post(path, data)

        r = response.json()
        self.assertEqual(response.status_code, 400)

        self.assertIn('first_name', r)
        self.assertIn('last_name', r)


    def test_registration_easy_password(self):
        client = APIClient()
        path = '/auth/users/'
        data = {
            'email': 'jdoe@example.com',
            'password': 'abcd1234',
            'first_name': 'John',
            'last_name': 'Doe',
        }
        response = client.post(path, data)
        r = response.json()
        self.assertEqual(response.status_code, 400)
        self.assertEqual(r, {'password': ['This password is too common.']})


    def test_registration_duplicate_email_case(self):
        client = APIClient()
        path = '/auth/users/'
        data = {
            'email': 'jdoe@example.com',
            'first_name': 'John',
            'last_name': 'Doe',
            'password': '3BQBMgR.e6q1k',
        }
        response = client.post(path, data)
        r = response.json()
        self.assertEqual(response.status_code, 201)


        data['email'] = 'JDOE@example.com'
        response = client.post(path, data)
        r = response.json()
        self.assertEqual(response.status_code, 400)
        self.assertIn('email', r)

        count = User.objects.filter(email__icontains='jdoe@example.com').count()
        self.assertEqual(1, count)


    def test_registration_successful(self):
        client = APIClient()
        path = '/auth/users/'
        data = {
            'email': 'jdoe@example.com',
            'first_name': 'John',
            'last_name': 'Doe',
            'password': '3BQBMgR.e6q1k',
        }
        response = client.post(path, data)
        r = response.json()
        self.assertEqual(response.status_code, 201)

        count = User.objects.filter(email='jdoe@example.com').count()
        self.assertEqual(1, count)

        user = User.objects.filter(email='jdoe@example.com').first()

