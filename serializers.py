from djoser.serializers import UserCreateSerializer as djUserCreateSerializer
from django.contrib.auth import get_user_model
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import serializers

from .models import CUser

User = get_user_model()


class UserCreateSerializer(djUserCreateSerializer):
    def validate_email(self, value):
        x = CUser.objects.filter(email__iexact=value).count()
        if x > 0:
            raise serializers.ValidationError(
                'A user with that email address already exists.')
        return value.lower()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = tuple(User.REQUIRED_FIELDS) + (
            User._meta.pk.name,
            User.USERNAME_FIELD,
            'is_staff',
            'is_superuser',
        )
        read_only_fields = (User.USERNAME_FIELD, 'is_staff', 'is_superuser')

    def update(self, instance, validated_data):
        email_field = get_user_email_field_name(User)
        if settings.SEND_ACTIVATION_EMAIL and email_field in validated_data:
            instance_email = get_user_email(instance)
            if instance_email != validated_data[email_field]:
                instance.is_active = False
                instance.save(update_fields=['is_active'])
        return super(UserSerializer, self).update(instance, validated_data)


class CUserObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super(CUserObtainPairSerializer, cls).get_token(user)

        # Add custom claims
        token['is_staff'] = user.is_staff

        return token


class CUserObtainPairView(TokenObtainPairView):
    serializer_class = CUserObtainPairSerializer
  
